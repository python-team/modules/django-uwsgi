Source: django-uwsgi
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Dominik George <natureshadow@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Standards-Version: 4.2.1
Homepage: https://edugit.org/AlekSIS/libs/django-uwsgi-ng
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-uwsgi
Vcs-Git: https://salsa.debian.org/python-team/packages/django-uwsgi.git
Testsuite: autopkgtest-pkg-python

Package: python-django-uwsgi-ng-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Conflicts: python-django-uwsgi-doc (<< 1.1.2~)
Replaces: python-django-uwsgi-doc (<< 1.1.2~)
Provides: python-django-uwsgi-doc
Description: uWSGI related tools for Django (Documentation)
 django-uwsgi provides several features for Django projects deployed to uWSGI:
 .
  * Admin page with uWSGI stats (options to reload/stop uWSGI, clear uWSGI
    cache)
  * uWSGI Cache Backend for Django
  * uWSGI Email Backend for Django(send emails via uWSGI's spooler)
  * Debug Panel for django-debug-toolbar (offers same functions as admin page)
  * Django template loader for embedded into uWSGI files
  * Django Management Command runuwsgi (with live autoreload when DEBUG is True)
  * uWSGI config generator
  * Django CBV Mixins based on uWSGI decorators
 .
 This package contains the documentation.

Package: python3-django-uwsgi-ng
Architecture: all
Depends:
 python3-django,
 uwsgi-plugin-python3,
 ${misc:Depends},
 ${python3:Depends},
Enhances:
 python3-django-debug-toolbar,
Conflicts: python3-django-uwsgi (<< 1.1.2~)
Replaces: python3-django-uwsgi (<< 1.1.2~)
Provides: python3-django-uwsgi
Description: uWSGI related tools for Django (Python3 version)
 django-uwsgi provides several features for Django projects deployed to uWSGI:
 .
  * Admin page with uWSGI stats (options to reload/stop uWSGI, clear uWSGI
    cache)
  * uWSGI Cache Backend for Django
  * uWSGI Email Backend for Django(send emails via uWSGI's spooler)
  * Debug Panel for django-debug-toolbar (offers same functions as admin page)
  * Django template loader for embedded into uWSGI files
  * Django Management Command runuwsgi (with live autoreload when DEBUG is True)
  * uWSGI config generator
  * Django CBV Mixins based on uWSGI decorators
 .
 This package contains the Python 3 version of the library.

Package: python-django-uwsgi-doc
Section: oldlibs
Architecture: all
Depends:
 ${misc:Depends},
 python-django-uwsgi-ng-doc
Description: uWSGI related tools for Django (Documentation, transitional dummy package)
 django-uwsgi provides several features for Django projects deployed to uWSGI:
 .
  * Admin page with uWSGI stats (options to reload/stop uWSGI, clear uWSGI
    cache)
  * uWSGI Cache Backend for Django
  * uWSGI Email Backend for Django(send emails via uWSGI's spooler)
  * Debug Panel for django-debug-toolbar (offers same functions as admin page)
  * Django template loader for embedded into uWSGI files
  * Django Management Command runuwsgi (with live autoreload when DEBUG is True)
  * uWSGI config generator
  * Django CBV Mixins based on uWSGI decorators
 .
 This package is a transitional package. It installs python-django-uwsgi-ng-doc.

Package: python3-django-uwsgi
Section: oldlibs
Architecture: all
Depends:
 ${misc:Depends},
 python3-django-uwsgi-ng
Description: uWSGI related tools for Django (Python3 version, transitional dummy package)
 django-uwsgi provides several features for Django projects deployed to uWSGI:
 .
  * Admin page with uWSGI stats (options to reload/stop uWSGI, clear uWSGI
    cache)
  * uWSGI Cache Backend for Django
  * uWSGI Email Backend for Django(send emails via uWSGI's spooler)
  * Debug Panel for django-debug-toolbar (offers same functions as admin page)
  * Django template loader for embedded into uWSGI files
  * Django Management Command runuwsgi (with live autoreload when DEBUG is True)
  * uWSGI config generator
  * Django CBV Mixins based on uWSGI decorators
 .
 This package is a transitional package. It installs python3-django-uwsgi-ng.
