Welcome to Django-uWSGI's documentation!
========================================

You can view the code of this project or fork it (please, send pull requests), at `EduGit <https://edugit.org/AlekSIS/libs/django-uwsgi-ng>`_.


.. toctree::
   :maxdepth: 2

   features
   installation
   configuration
   decorators
   email
   cache
   command
   emperor
   integrations
   screenshots
   todo
   changelog
   contributing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
